# Creating a Dynamic Webpage using JS

## Description

This is the second assignemnt in the frontend course from Noroff.
The main purpose of this assignment was learning to build a dynamic webpage using 'vanilla' JavaScript.

We had to make a simple banking application, which has some functionality for taking a loan, working, purchasing a laptops etc.
In the assignment we were provided with a wireframe, which I decided to follow when creating my webpage.

## Description

Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Installation

To use this project, you can simply use git to clone the repository. Make sure to install "Live Server" or similar tools to preview and test the HTML, CSS and JavaScript code.

## Contributors

The project as a whole was developed by Tore Kamsvåg.
