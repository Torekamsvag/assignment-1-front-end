// Creating DOM elements to interact with the HTML elements
const loanButtonElement = document.getElementById("loan-button");
const loanAmountElement = document.getElementById("loan-amount");
const outstandingLoanElement = document.getElementById("outstanding-loan");
const salaryElement = document.getElementById("salary-amount");
const bankButtonElement = document.getElementById("bank-button");
const workButtonElement = document.getElementById("work-button");
const balanceElement = document.getElementById("balance");
const repayLoanElement = document.getElementById("repay-loan-button");
const laptopsElement = document.getElementById("laptops");
const specificationsElement = document.getElementById("specs");
const laptopPriceElement = document.getElementById("laptop-price");
const laptopDescElement = document.getElementById("laptop-desc");
const imageElement = document.getElementById("pc-image");
const laptopNameElement = document.getElementById("laptop-name");
const buyButtonElement = document.getElementById("buy-button");

// Setting initial values
let balance = 200;
let outstandingLoan = 0;
let pay = 0;
const rootURL = "https://hickory-quilled-actress.glitch.me/";

// Function to get a loan
function getLoan() {
  // Check if there's no outstanding loan
  if (outstandingLoan === 0) {
    let loanAmount = prompt("Enter loan amount: ");
    loanAmount = parseInt(loanAmount);
    // Check if loan amount is positive and less than or equal to double the balance
    if (loanAmount <= balance * 2 && loanAmount > 0) {
      outstandingLoan = loanAmount;
      // Update the value of the loan amount
      loanAmountElement.innerHTML = outstandingLoan + " Kr";
      // Displaying the outstanding loan element
      outstandingLoanElement.style.display = "block";
      // Displaying the repay loan element
      repayLoanElement.style.display = "block";
      alert(`Loan approved! Your outstanding loan is: ${outstandingLoan}`);
    } else {
      alert(
        "Loan request denied. You can only get a loan that's less than or equal than double of your bank balance"
      );
    }
  } else {
    alert(
      "You have an outstanding loan. Please repay your loan before getting another loan"
    );
  }
}

// Function that increments the value of pay by 100
function getPayment() {
  pay += 100;

  // Update the value of pay
  salaryElement.innerHTML = pay + " Kr";
}

// Function to handle transfers from pay to bank balance
function transferPayment() {
  let loanPayment = pay * 0.1;

  if (loanPayment > outstandingLoan) {
    pay -= outstandingLoan;
    balance += pay;
    // reset the loan payment, outstanding loan and pay amount
    loanPayment = 0;
    outstandingLoan = 0;
    pay = 0;
  } else if (outstandingLoan > 0) {
    pay -= loanPayment;
    outstandingLoan -= loanPayment;
    balance += pay;
    pay = 0;
    loanAmountElement.innerHTML = outstandingLoan + " Kr";
  } else {
    balance += pay;
    pay = 0;
  }

  balanceElement.innerHTML = balance + " Kr";
  salaryElement.innerHTML = pay + " Kr";

  // Hides the outstanding loan and repay loan elements if the outstanding loan is zero
  if (outstandingLoan === 0) {
    outstandingLoanElement.style.display = "none";
    repayLoanElement.style.display = "none";
  }
}

// Function to handle loan repayments directly from pay
function repayLoan() {
  if (pay > outstandingLoan) {
    pay -= outstandingLoan;
    balance += pay;
    outstandingLoan = 0;
    pay = 0;
    balanceElement.innerHTML = balance + " Kr";
  } else {
    outstandingLoan -= pay;
    pay = 0;
  }
  salaryElement.innerHTML = pay + " Kr";
  loanAmountElement.innerHTML = outstandingLoan + " Kr";

  if (outstandingLoan === 0) {
    outstandingLoanElement.style.display = "none";
    repayLoanElement.style.display = "none";
  }
}

// Fetch request to an API
fetch("https://hickory-quilled-actress.glitch.me/computers")
  .then((response) => response.json()) // Converting the response to JSON
  .then((data) => (laptops = data)) // Saving the data in a new variable called `laptops`
  .then((laptops) => addLaptops(laptops));

const addLaptops = (laptops) => {
  laptops.forEach((x) => addLaptop(x));
};

const addLaptop = (laptop) => {
  const laptopElement = document.createElement("option");
  laptopElement.value = laptop.id;
  laptopElement.appendChild(document.createTextNode(laptop.title));
  laptopsElement.appendChild(laptopElement);
};

// Function to update information when a laptop is selected
const changeLaptop = (e) => {
  const selectedLaptop = laptops[e.target.selectedIndex];
  let featuresString = "";
  for (const spec of selectedLaptop.specs) {
    featuresString += spec + "\n";
  }
  specificationsElement.innerText = featuresString;
  laptopPriceElement.innerText = parseInt(selectedLaptop.price) + " Kr";
  laptopDescElement.innerText = selectedLaptop.description;
  imageElement.src = rootURL + selectedLaptop.image;
  imageElement.alt = `Should display an image of ${selectedLaptop.title} laptop`;
  laptopNameElement.innerText = selectedLaptop.title;
};

// Function that handles the purchase of a laptop
function buyLaptop() {
  const selectedLaptop = laptops[laptopsElement.selectedIndex];
  if (balance < selectedLaptop.price) {
    alert("Insufficient funds to purchase the selected laptop");
  } else {
    balance -= selectedLaptop.price;
    alert(`An order has been placed for the ${selectedLaptop.title}`);
    balanceElement.innerHTML = balance + " Kr";
  }
}

// Listen for events from HTML elements
loanButtonElement.addEventListener("click", getLoan);
workButtonElement.addEventListener("click", getPayment);
bankButtonElement.addEventListener("click", transferPayment);
repayLoanElement.addEventListener("click", repayLoan);
laptopsElement.addEventListener("change", changeLaptop);
buyButtonElement.addEventListener("click", buyLaptop);
